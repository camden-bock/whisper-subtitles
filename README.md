# Automatic SRT Generation for OGGs in Directory

I plan to expand this to handle multi-track audio, or multiple audio files synchronzied in one folder.

This repository uses [OpenAI's Whisper](https://openai.com/blog/whisper) to generate subtitle files for any youtube video.

## Installation

To get started, you'll need Python 3.7 or newer. Install the binary by running the following command:

    pip install git+https://github.com/m1guelpf/yt-whisper.git

You'll also need to install [`ffmpeg`](https://ffmpeg.org/), which is available from most package managers:

```bash
# on Ubuntu or Debian
sudo apt update && sudo apt install ffmpeg

# on MacOS using Homebrew (https://brew.sh/)
brew install ffmpeg

# on Windows using Chocolatey (https://chocolatey.org/)
choco install ffmpeg
```

## Usage

Run main.py after setting the model and directory options.

## License

This script is open-source and licensed under the MIT License. For more details, check the [LICENSE](LICENSE) file.
