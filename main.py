import whisper
import utils
import ffmpeg

videoPath = 'Group3_part2_hq.mp4'

stream = ffmpeg.input(videoPath).audio
output = ffmpeg.output(stream, 'Group3_part2_audioStream.ogg')
ffmpeg.run(output, quiet=True, overwrite_output=True)

model = whisper.load_model("medium.en")

utils.local_srt('Group3_part2_audioStream.ogg', model)
